module Spree
  AppConfiguration.class_eval do
    preference :kashflow_user, :string
    preference :kashflow_password, :string
    preference :kashflow_shipping_code, :string, default: 190
    preference :kashflow_sale_of_goods_code, :string, default: 100
    preference :kashflow_payment_bank_account, :string
    preference :kashflow_payment_method, :string
  end
end