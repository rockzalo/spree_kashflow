module Spree
  Payment.class_eval do
    def to_kashflow
      {
        pay_invoice: order.kashflow_number,
        pay_amount: amount.to_f,
        pay_method: Spree::Config[:kashflow_payment_method],
        pay_account: Spree::Config[:kashflow_payment_bank_account]
      }.delete_if {|k,v| !v.present?}
    end
  end
end