// Placeholder manifest file.
// the installer will append this file to the app vendored assets here: vendor/assets/javascripts/spree/backend/all.js'

Spree.routes.kashflow_bank_accounts = Spree.adminPathFor(
  "general_settings/kashflow_bank_accounts"
);

Spree.routes.kashflow_payment_methods = Spree.adminPathFor(
  "general_settings/kashflow_payment_methods"
);

$(function() {
  $(".kashflow-select2").each(function() {
    var $this = $(this);
    $this.select2({
      initSelection: initSelection,
      ajax: ajaxObject($this.data("url")),
      formatResult: formatResult,
      formatSelection: formatSelection
    });
  });

  function initSelection(element, callback) {
    var id = $(element).val();
    var url = $(element).data("url");
    if (id !== "") {
      $.ajax(url, {
        dataType: "json"
      }).done(function(data) {
        object = data.find(function(o) {
          return o.id === id;
        });
        callback(object);
      });
    }
  }

  function ajaxObject(url) {
    return {
      url: url,
      dataType: "json",
      results: function(data) {
        return {
          results: data
        };
      },
      cache: true
    };
  }

  function formatResult(object) {
    return object.text;
  }

  function formatSelection(object) {
    return object.text;
  }
});
