class PlaceOrderKashflow
  include Interactor::Organizer

  organize KashflowAuthenticate, SetupCurrencies, CreateCustomer, CreateOrder, CreateItems, CreatePayment
end
