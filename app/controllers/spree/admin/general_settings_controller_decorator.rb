module Spree
  module Admin
    GeneralSettingsController.class_eval do
      def kashflow_bank_accounts
        @accounts = Rails.cache.fetch("kashflow_bank_accounts", expires_in: 1.hour) do
          KashflowAuthenticate.call.client.get_bank_accounts
        end

        formatted_accounts = @accounts.map {|a| {id: a.account_id, text: a.account_name }}

        render json: formatted_accounts
      end

      def kashflow_payment_methods
        @payment_methods = Rails.cache.fetch("kashflow_payment_methods", expires_in: 1.hour) do
          KashflowAuthenticate.call.client.get_inv_pay_methods
        end

        formatted_payment_methods = @payment_methods.map {|m| {id: m.method_id, text: m.method_name }}

        render json: formatted_payment_methods
      end
    end
  end
end