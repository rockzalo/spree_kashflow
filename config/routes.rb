Spree::Core::Engine.add_routes do
  namespace :admin do
    resource :general_settings do
      collection do
        get :kashflow_bank_accounts
        get :kashflow_payment_methods
      end
    end

    resources :orders do
      member do
        post :sync_kashflow
      end
    end
  end
end
